package com.seejoke.swagger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * 启动入口
 *
 * @author yangzhongying
 * @date 2019/2/20 14:43
 */
@SpringBootApplication
@EnableWebMvc
public class StartApplication {

  private static final Logger logger = LoggerFactory.getLogger(StartApplication.class);

  public static void main(String[] args) {
    SpringApplication.run(StartApplication.class, args);
  }
}
