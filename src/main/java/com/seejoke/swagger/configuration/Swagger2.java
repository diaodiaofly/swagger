package com.seejoke.swagger.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author yangzhongying
 * @date 2019/2/20 14:39
 */
@Configuration
@EnableSwagger2
public class Swagger2 {
  @Bean
  public Docket createRestApi() {

    ApiInfo apiInfo =
        new ApiInfoBuilder()
            .title("Api项目接口文档")
            .description("Api项目的接口文档，符合Restful Api")
            .version("1.0")
            .build();

    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo)
        .select()
        // 以扫描包的方式
        .apis(RequestHandlerSelectors.basePackage("com.seejoke.swagger.controller"))
        .paths(PathSelectors.any())
        .build();
  }
}
